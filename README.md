# OldNBuggy
Gentoo overlay with ebuilds for software that:
1. is either too old and was removed from the main tree;
2. is too new/unpopular to be in the main tree.

## app-misc/rogauracore/rogauracore-1.4.ebuild
RGB keyboard control for Asus ROG laptops.

## games-emulation/retroarch/retroarch-99999999.ebuild
Frontend for emulators, game engines and media players. Depends from other ebuilds from menelkir overlay.

## media-sound/rakarrack/rakarrack-0.6.1.ebuild
Modified from an old ebuild that was removed from the tree.

## net-libs/decsync/decsync-1.9.0.ebuild, dev-python/libdecsync/libdecsync-1.7.1.ebuild and dev-python/radicale-storage-decsync/radicale-storage-decsync-1.2.2.ebuild
Tools for syncing with DecSync.

## net-misc/nextcloud-client/nextcloud-client-2.3.3.ebuild
The last version that doesn't make me type my username and passwd every single time I open it, or that does not rely on gnome-keyring.

