# Copyright 2021-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
PYTHON_COMPAT=( python3_{10..12} )
DISTUTILS_USE_PEP517=setuptools
inherit distutils-r1 pypi

DESCRIPTION="Python3 bindings for libdecsync"
HOMEPAGE="https://github.com/39aldo39/libdecsync-bindings-python3"

LICENSE="LGPL-2+"
SLOT="0"
KEYWORDS="~amd64"
