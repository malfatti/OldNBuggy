# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{10..11} )
inherit distutils-r1 pypi

DESCRIPTION="Abjad is a Python API for building LilyPond files."
HOMEPAGE="https://abjad.github.io"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="
	media-sound/lilypond
	dev-python/ply[${PYTHON_USEDEP}]
	dev-python/roman[${PYTHON_USEDEP}]
	dev-python/uqbar[${PYTHON_USEDEP}]
"
