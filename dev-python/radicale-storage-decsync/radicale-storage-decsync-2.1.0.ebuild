# Copyright 2021-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{10..12} )
inherit distutils-r1 pypi

DESCRIPTION="DecSync storage plugin for Radicale"
HOMEPAGE="https://github.com/39aldo39/Radicale-DecSync"

LICENSE="GPL-3+"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="
	>=dev-python/libdecsync-2.0.0[${PYTHON_USEDEP}]
	>=www-apps/radicale-3[${PYTHON_USEDEP}]
"

# S="${WORKDIR}/${PN//-/_}-${PV}"
