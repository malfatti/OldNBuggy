# Copyright 2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8
DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{8..12} )
inherit distutils-r1 pypi

DESCRIPTION="Tools for building documentation with Sphinx, Graphviz and LaTeX"
HOMEPAGE="https://github.com/josiah-wolf-oberholtzer/uqbar"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64"
