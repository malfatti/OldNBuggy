# Copyright 2021 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7
inherit autotools

DESCRIPTION="RGB keyboard control for Asus ROG laptops"
HOMEPAGE="https://github.com/wroberts/rogauracore"
SRC_URI="https://github.com/wroberts/${PN}/archive/refs/tags/${PV}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="~amd64 ~x86"

src_prepare() {
   default
   eautoreconf
}
